package shunting_yard;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Stack;

class RegExp {

    public static final String NUMBER = "^\\d+(\\.{1}\\d+)?$";
    public static final String OPERATOR = "^[-+/*\\(\\)]{1}$";
    public static final String EXPR_SPLIT = "(?<=[-+*/()])|(?=[-+*/()])";
}

/**
 * The Operator class
 *
 * @author youssef
 */
class Operator {

    private char character;

    public Operator(char character) {
        assert character == '+'
                || character == '-'
                || character == '/'
                || character == '*'
                || character == '('
                || character == ')';
        this.character = character;
    }

    public char getCharacter() {
        return character;
    }

    public void setCharacter(char character) {
        this.character = character;
    }

    public int precedence() {
        switch (character) {
            case '+':
                return 2;
            case '-':
                return 2;
            case '*':
                return 3;
            case '/':
                return 3;
            case '(':
            case ')':
                return 0;
            default:
                assert false;
        }
        return 0;
    }

    public double revCalculate(double num1, double num2) {
        assert character != '(' && character != ')';
        System.out.println(num2 + " " + this.toString() + " " + num1);
        switch (character) {
            case '+':
                return num2 + num1;
            case '-':
                return num2 - num1;
            case '*':
                return num2 * num1;
            case '/':
                return num2 / num1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return new String(new char[]{character});
    }
}

class Algorithms {

    public static ArrayList<String> shuntingYard(String expression) {
        ArrayList<String> output = new ArrayList<>();
        Stack<Operator> stack = new Stack<>();

        // Split the expression to tokens
        String[] tokens = expression.replace(" ", "").split(RegExp.EXPR_SPLIT);

        // Process the tokens
        for (String token : tokens) {
            if (token.matches(RegExp.NUMBER)) {
                output.add(token);
            } else if (token.matches(RegExp.OPERATOR)) { //< operator
                Operator operator = new Operator(token.charAt(0));

                if (!stack.empty()) {
                    while (!stack.empty()
                            && (stack.peek().getCharacter() == '('
                            || stack.peek().precedence() >= operator.precedence())) {
                        output.add(stack.pop().toString());
                    }
                }

                //< remove matching paratehsis
                if (operator.getCharacter() == ')') {
                    stack.pop();
                } else {
                    stack.push(operator);
                }
            }
        }

        while (!stack.empty()) {
            output.add(stack.pop().toString());
        }

        ArrayList<String> result = (ArrayList<String>) output.clone();

        return result;
    }

    public static double reversePolishNotation(ArrayList<String> expression) {
        Stack<Double> stack = new Stack<>();
        ArrayList<String> rpn = (ArrayList<String>) expression.clone();

        // Push numbers into the stack         
        while (rpn.get(0).matches(RegExp.NUMBER)) {
            stack.push(Double.parseDouble(rpn.get(0)));
            rpn.remove(0);
        }

        // Calculate
        while (rpn.size() > 0) {
            // Operator
            if (rpn.get(0).matches(RegExp.OPERATOR)) {
                Operator operator = new Operator(rpn.get(0).charAt(0));
                stack.push(operator.revCalculate(stack.pop(), stack.pop()));
            } // Number 
            else {
                Operator operator = new Operator(rpn.get(1).charAt(0));
                double currentNumber = Double.parseDouble(rpn.get(0));
                stack.push(operator.revCalculate(currentNumber, stack.pop()));
                rpn.remove(1); //< remove operator
            }
            rpn.remove(0);
        }
        
        return stack.peek();
    }
}

/**
 * The class that contains main
 *
 * @author Youssef BOUHJRIA
 */
public class Shunting_yard {

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);

        // Loop  
        while (true) {
            System.out.print(">>> ");
            String command = scn.nextLine();

            if (command.equals("exit")) {
                break;
            } else {
                ArrayList rpn = Algorithms.shuntingYard(command);
                System.out.println(Algorithms.reversePolishNotation(rpn));
            }
        }
    }
}
